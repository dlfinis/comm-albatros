#ifndef _UTIL_H_
#define _UTIL_H_

#include <queue>
#include <boost/thread/thread.hpp>
#include <boost/thread/locks.hpp>

/**
 * Thread-safe synchronized FIFO queue.
 */
template<typename DataType>
class BlockingQueue {
public:
	/**
	 * Construct a thread-safe synchronized FIFO queue.
	 */
	BlockingQueue();
	/**
	 * Destroy a thread-safe synchronized FIFO queue.
	 */
	~BlockingQueue();
	/**
	 * Put an element (at the end of the queue). This is a non-blocking operation.
	 * @param element Element to be pushed.
	 */
    void push(DataType const& element);
    /**
     * Get an element (from the head of the queue), and remove it from the queue. This is a blocking operation.
     * @param element Element retrieved from the head of the queue.
     * @return True if an element was popped, false if the operation was interrupted.
     */
    bool pop(DataType& element);
    /**
     * Stop all pop operations.
     */
    void stopAll();
    /**
     * Return the number of elements in the queue.
     * @return Number of elements in the queue.
     */
    int size() const;
    bool debug; // JMR
private:
    std::queue<DataType> queue;
    boost::mutex mutex;
    boost::condition_variable condition;
    bool stop;
};

template<typename DataType> BlockingQueue<DataType>::BlockingQueue() :
		stop(false)
{
	debug = false; // JMR
}

template<typename DataType> BlockingQueue<DataType>::~BlockingQueue()
{
}

template<typename DataType> void BlockingQueue<DataType>::push(DataType const& element)
{
	if (debug) std::cerr << "PUSH1" << std::endl;
	boost::unique_lock<boost::mutex> m(mutex);
	queue.push(element);
	condition.notify_one();
	m.unlock();
	if (debug) std::cerr << "PUSH2" << std::endl;
}

template<typename DataType> bool BlockingQueue<DataType>::pop(DataType& element)
{
	boost::unique_lock<boost::mutex> m(mutex);
	if (debug) std::cerr << "POP1" << std::endl;
	while (! stop && queue.empty()) {
		if (debug) std::cerr << "WAIT" << std::endl;
		condition.wait(m);
	}
	if (! queue.empty()) {
		element = queue.front();
		queue.pop();
		if (debug) std::cerr << "POP2" << std::endl;
		return true;
	}
	else {
		if (debug) std::cerr << "RETORNO FALSO" << std::endl;
		return false;
	}
}

template<typename DataType> void BlockingQueue<DataType>::stopAll()
{
	stop = true;
	condition.notify_all();
}

template<typename DataType> int BlockingQueue<DataType>::size() const
{
	return queue.size();
}


#endif

