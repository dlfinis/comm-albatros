#include <string>
#include <iostream>
#include "crypto++/osrng.h"
#include "crypto++/default.h"
#include "crypto++/hex.h"
#include "crypto++/aes.h"
#include "crypto++/filters.h"


using namespace std;
using namespace CryptoPP;


class SecurityMessage {
private:


    byte key[ CryptoPP::AES::DEFAULT_KEYLENGTH ], iv[ CryptoPP::AES::BLOCKSIZE ];

    //
    // String and Sink setup
    //



public:
    std::string pack (std::string raw_text){


    memset( key, 0xf4, CryptoPP::AES::DEFAULT_KEYLENGTH );
    memset( iv, 0xf4, CryptoPP::AES::BLOCKSIZE );

    std::string plaintext  = raw_text;
    std::string ciphertext;


    #if DEBUG_SMSG > 1
    //
    // Dump Plain Text
    //
    std::cout << "------------------------------------ " << std::endl;
    std::cout << "[SMSG] Plain Text (" << plaintext.size() << " bytes)" << std::endl;
    std::cout << plaintext;
    std::cout << std::endl << std::endl;
    #endif // LOG
    std::cout << "------------------------------------ " << std::endl;
    //
    // Create Cipher Text
    //

    CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
    CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption( aesEncryption, iv );

    CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink( ciphertext ) );
    stfEncryptor.Put( reinterpret_cast<const unsigned char*>( plaintext.c_str() ), plaintext.length() + 1 );
    stfEncryptor.MessageEnd();

    #if DEBUG_SMSG > 1
    //
    // Dump Cipher Text
    //
    std::cout << "------------------------------------ " << std::endl;
    std::cout << "[SMSG] Encrypted Text: (" << ciphertext.size() << " bytes)" << std::endl;

    for( int i = 0; i < ciphertext.size(); i++ ) {

        std::cout << "0x" << std::hex << (0xFF & static_cast<byte>(ciphertext[i])) << " ";
    }
    std::cout << "------------------------------------ " << std::endl;
    std::cout << std::endl << std::endl;

    #endif // LOG


    return ciphertext;
    }

    std::string unpack(std::string ciphertext){

    std::string decryptedtext;


    memset( key, 0xf4, CryptoPP::AES::DEFAULT_KEYLENGTH );
    memset( iv, 0xf4, CryptoPP::AES::BLOCKSIZE );

    //
    // Decrypt
    //
    CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
    CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption( aesDecryption, iv );

    CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink( decryptedtext ) );
    stfDecryptor.Put( reinterpret_cast<const unsigned char*>( ciphertext.c_str() ), ciphertext.size() );
    stfDecryptor.MessageEnd();

    #if DEBUG_SMSG > 1
    //
    // Dump Decrypted Text
    //
    std::cout << "------------------------------------ " << std::endl;
    std::cout << "[SMSG] Decrypted Text: " << std::endl;
    std::cout << decryptedtext;
    std::cout << std::endl << std::endl;
    std::cout << "------------------------------------ " << std::endl;
    std::cout << std::endl << std::endl;
    #endif //LOG

    return decryptedtext;
    }

    void printHex(std::string hex_text){


        std::cout << "  Hex Text (" << static_cast<int>(hex_text.size()) << " bytes)" << std::endl;

        for( int i = 0; i < hex_text.size(); i++ ) {

            std::cout << "  0x" << std::hex << (0xFF & static_cast<byte>(hex_text[i])) << " ";
        }

        std::cout << std::endl << std::endl;

    }

    void test(std::string example){
    //Key and IV setup
    //AES encryption uses a secret key of a variable length (128-bit, 196-bit or 256-
    //bit). This key is secretly exchanged between two parties before communication
    //begins. DEFAULT_KEYLENGTH= 16 bytes
    byte key[ CryptoPP::AES::DEFAULT_KEYLENGTH ], iv[ CryptoPP::AES::BLOCKSIZE ];
    memset( key, 0xf4, CryptoPP::AES::DEFAULT_KEYLENGTH );
    memset( iv, 0xf4, CryptoPP::AES::BLOCKSIZE );

    //
    // String and Sink setup
    //
    std::string plaintext = example;
    std::string ciphertext;
    std::string decryptedtext;

    //
    // Dump Plain Text
    //
    std::cout << "Plain Text (" << plaintext.size() << " bytes)" << std::endl;
    std::cout << plaintext;
    std::cout << std::endl << std::endl;

    //
    // Create Cipher Text
    //
    CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
    CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption( aesEncryption, iv );

    CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink( ciphertext ) );
    stfEncryptor.Put( reinterpret_cast<const unsigned char*>( plaintext.c_str() ), plaintext.length() + 1 );
    stfEncryptor.MessageEnd();

    //
    // Dump Cipher Text
    //
    std::cout << "Cipher Text (" << ciphertext.size() << " bytes)" << std::endl;

    for( int i = 0; i < ciphertext.size(); i++ ) {

        std::cout << "0x" << std::hex << (0xFF & static_cast<byte>(ciphertext[i])) << " ";
    }

    std::cout << std::endl << std::endl;

    //
    // Decrypt
    //
    CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
    CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption( aesDecryption, iv );

    CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink( decryptedtext ) );
    stfDecryptor.Put( reinterpret_cast<const unsigned char*>( ciphertext.c_str() ), ciphertext.size() );
    stfDecryptor.MessageEnd();

    //
    // Dump Decrypted Text
    //
    std::cout << "Decrypted Text: " << std::endl;
    std::cout << decryptedtext;
    std::cout << std::endl << std::endl;

    }
};
