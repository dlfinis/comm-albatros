/**
 * Anonymous communication.
 *
 * @author Javier Martín Rueda (jmrueda@diatel.upm.es)
 * @version 1.0 (18-jun-2014)
 */

#ifndef _ANONYMOUS_COMMUNICATOR_H_
#define _ANONYMOUS_COMMUNICATOR_H_

#include <list>
#include "Message.h"

/**
 * Service type identifier used to select relevant broadcast messages.
 */
typedef u_int8_t ServiceType;

typedef std::list<MessageBuffer*> MessageBufferList;

/**
 * This is a base virtual class for the actual classes used to perform anonymous communication (both sending and receiving messages).
 */

class AnonymousCommunicator {
public:
	/**
	 * Destroy an anonymous communicator, closing communication and releasing other resources.
	 */
	virtual ~AnonymousCommunicator();

	/**
	 * Broadcast an anonymous message.
	 * @param message Message to be broadcasted.
	 * @throws runtime_error Error in some network system call.
	 */
	virtual void broadcast(const Message* const message) = 0;
};

#endif

