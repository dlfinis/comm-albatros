/**
 * Anonymous communication through ethernet frames.
 *
 * @author Javier Martín Rueda (jmrueda@diatel.upm.es)
 * @version 1.0 (18-jun-2014)
 */

#include "EthernetAnonymousCommunicator.h"
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <net/ethernet.h>

#if defined(__FreeBSD__)
#include <sys/ioctl.h>
#endif

#include <netinet/in.h>
#include <arpa/inet.h>


#if defined(__FreeBSD__)
#include <net/if.h>
#include <net/bpf.h>
#include <fcntl.h>
#endif

#include <errno.h>
#include <unistd.h>
#include <string.h>

#include <stdlib.h>
#ifdef SIMULATE_COMM_ERRORS
#include <time.h>
#endif
#ifdef __FreeBSD__
#include <pcap-int.h>
#endif

// Ethertype to use in broadcast ethernet frames. See http://standards.ieee.org/develop/regauth/ethertype/eth.txt
// 88b5 is defined as IEEE Std 802 - Local Experimental Ethertype 1
#define AC_ETHERTYPE 0x88b5
// #define AC_ETHERTYPE 0x0806

LowLevelEthernetAnonymousCommunicatorMap EthernetAnonymousCommunicator::llEACMap;
boost::mutex EthernetAnonymousCommunicator::llEACMapMutex;

LowLevelEthernetAnonymousCommunicator::~LowLevelEthernetAnonymousCommunicator()
{
	while (sendingQueue.size() > 0) {
		MessageBuffer* mb;
		sendingQueue.pop(mb);
		delete (u_int8_t*) mb->data;
		delete mb;
	}
	boost::lock_guard<boost::mutex> m(receivingQueuesMutex);
	MessageBufferQueuesMap::iterator mbqmi;
	for (mbqmi = receivingQueues.begin(); mbqmi != receivingQueues.end(); mbqmi++) {
		MessageBufferQueueList* mbql = mbqmi->second;
		delete mbql;
	}
}

void LowLevelEthernetAnonymousCommunicator::broadcast(const struct ether_header& etherHeader, const EACHeader& eacHeader, const MessageBuffer* mb)
{
	const MessageBuffer* mb0;
	MessageBuffer* mymb;
	u_int8_t* aux;

	// Add up total length of all message chunks and headers and allocate a buffer to assemble the message.
	mymb = new MessageBuffer;
	mymb->length = sizeof(struct ether_header) + sizeof(EACHeader);
	mb0 = mb;
	while (mb0 != 0) {
		mymb->length += mb0->length;
		mb0 = mb0->prev;
	}
	mymb->data = new u_int8_t[mymb->length];
	// Fill the buffer with headers and payload.
	memcpy(mymb->data, &etherHeader, sizeof(struct ether_header));
	aux = (u_int8_t*) mymb->data + sizeof(struct ether_header);
	memcpy(aux, &eacHeader, sizeof(EACHeader));
	aux = (u_int8_t*) mymb->data + mymb->length;
	mb0 = mb;
	while (mb0 != 0) {
		aux -= mb0->length;
		memcpy(aux, mb0->data, mb0->length);
		mb0 = mb0->prev;
	}

	// Push the buffer into the outgoing queue.
	sendingQueue.push(mymb);
}

void LowLevelEthernetAnonymousCommunicator::addReceivingQueue(const int serviceTag, MessageBufferQueue* const messageBufferQueue) {
	boost::lock_guard<boost::mutex> m(receivingQueuesMutex);
	MessageBufferQueueList* mbql;

	if (receivingQueues.count(serviceTag) == 1)
		mbql = receivingQueues[serviceTag];
	else {
		mbql = new MessageBufferQueueList;
		receivingQueues[serviceTag] = mbql;
	}
	mbql->push_back(messageBufferQueue);
}

void LowLevelEthernetAnonymousCommunicator::dropReceivingQueue(MessageBufferQueue* const messageBufferQueue) {
	boost::lock_guard<boost::mutex> m(receivingQueuesMutex);
	MessageBufferQueueList* mbql;
	bool dropped = false;

	MessageBufferQueuesMap::iterator mbqmi, mbqmi2;
	for (mbqmi = receivingQueues.begin(); ! dropped && mbqmi != receivingQueues.end(); mbqmi++) {
		mbql = mbqmi->second;
		MessageBufferQueueList::iterator mbqli;
		for (mbqli = mbql->begin(); ! dropped && mbqli != mbql->end(); mbqli++)
			if (*mbqli == messageBufferQueue) {
				mbql->erase(mbqli);
				dropped = true;
				mbqmi2 = mbqmi;
			}
	}
	if (dropped && mbql->size() == 0) {
		receivingQueues.erase(mbqmi2);
		delete mbql;
	}
}

void LowLevelEthernetAnonymousCommunicator::distributePacket(const void* const packet, unsigned int packet_length)
{
	EACHeader* eacHeader;
	const u_int8_t* payload;

	eacHeader = (EACHeader*) ((u_int8_t*) packet + sizeof(struct ether_header));
	payload = (u_int8_t*) eacHeader + sizeof(EACHeader);
	// Take into account only packets that belong to my service id.
	if (packet_length >= sizeof(struct ether_header) + sizeof(EACHeader) && receivingQueues.count(eacHeader->serviceTag) == 1) {
#ifdef DEBUG_EAC
		if (globalParameters.debugEAC > 2) {
			std::cerr << "[EAC] Received " << packet_length << " bytes. Packet dump follows: " << std::hex;
			const u_int8_t* auxP = (const u_int8_t *) packet;
			for (unsigned int i = 0; i < packet_length; i++, auxP++) {
				if (auxP == (u_int8_t*) eacHeader)
					std::cerr << "H>";
				else if (auxP == payload)
					std::cerr << "P>";
				std::cerr << (int) *auxP << ' ';
			}
			std::cerr << std::dec;
			std::cerr << std::endl;
		}
#endif
		{


			boost::lock_guard<boost::mutex> m(receivingQueuesMutex);
			MessageBufferQueueList::iterator mbqli;
			for (mbqli = receivingQueues[eacHeader->serviceTag]->begin(); mbqli != receivingQueues[eacHeader->serviceTag]->end(); mbqli++) {
				MessageBufferQueue* q = *mbqli;
				MessageBuffer* mb = new MessageBuffer;
				mb->length = ntohs(eacHeader->payloadDataLength);
				mb->data = new u_int8_t[mb->length];
				memcpy(mb->data, payload, mb->length);
				mb->prev = 0;
				q->push(mb);

				#if DEBUG_SMSG > 1
                std::cout << std::endl;
                std::cout << " ------------------------------" << std::endl;
//              std::cout << "  Message Receiving: " << " Length: " << mb->length <<  " Data: " << mb-> data <<std::endl;
                std::cout << "[SMSG] -->> Message Receiving << -- " <<std::endl;
                std::cout << " ------------------------------" << std::endl;
                #endif

			}
		}
	}
}

PCAPEthernetAnonymousCommunicator::PCAPEthernetAnonymousCommunicator(const std::string &networkInterface) :
		stopSendingThread(false)
{
	char pcap_errbuf[PCAP_ERRBUF_SIZE];
	pcap_errbuf[0]='\0';
	// Open the pcap device with a read timeout, so that we can detect termination requests.
	if ((pcapfd = pcap_open_live(networkInterface.c_str(),BUFSIZ,1,0,pcap_errbuf)) == NULL) {
		std::stringstream s;
		s << "Error opening LIBPCAP: " << pcap_errbuf;
		throw std::runtime_error(s.str());
	}
#ifdef DEBUG_EAC
	if (globalParameters.debugEAC > 0)
		if (pcap_errbuf[0] != '\0')
			std::cerr << "[EAC] LIBPCAP warning: " << pcap_errbuf << std::endl;
#endif
#ifdef SIMULATE_COMM_ERRORS
	if (globalParameters.EACFailureRate > 0)
		srand(time(NULL));
#endif
#ifdef __FreeBSD__
	// For some reason, pcap_loop does not seem to work properly on FreeBSD.
	// A way of making it work is activating immediate mode on the bpf file descriptor it uses.
	int option = 1;
	ioctl(pcapfd->fd, BIOCIMMEDIATE, &option);
#endif
	sendingThread = boost::thread(&PCAPEthernetAnonymousCommunicator::packetSender, this);
	receivingThread = boost::thread(&PCAPEthernetAnonymousCommunicator::packetReceiver, this);
}

PCAPEthernetAnonymousCommunicator::~PCAPEthernetAnonymousCommunicator()
{
	pcap_breakloop(pcapfd);
	receivingThread.join();
	stopSendingThread = true;
	sendingQueue.stopAll();
	sendingThread.join();
	pcap_close(pcapfd);
}

void PCAPEthernetAnonymousCommunicator::packetSender()
{
	MessageBuffer* mb;
	int ret;

	do {
		if (sendingQueue.pop(mb)) {

#ifdef DEBUG_EAC
			if (globalParameters.debugEAC > 0) {
				std::cerr << "[EAC] Broadcast dump (" << mb->length << "B): ";
				std::cerr << std::hex;
				u_int8_t* aux = (u_int8_t*) mb->data;
				for (unsigned int p = 0; p < mb->length; p++, aux++)
					std::cerr << (int) *aux << " ";
				std::cerr << std::dec;
				std::cerr << std::endl;
			}
#endif

			ret = pcap_inject(pcapfd, mb->data, mb->length);
#ifndef __FreeBSD__
			// Messages injected by libpcap are not received by libpcap on the same host in some operating systems. Therefore, we
			// add the message to an "own messages" list, to be processed by the receiving thread.
			// FreeBSD does receive it, so we don't need to add it to my "own messages" list.
			distributePacket(mb->data, mb->length);
#endif
			delete (u_int8_t*) mb->data;
			delete mb;
			// TODO: in case of error, maybe we can try to restart PCAP, or maybe we can discard the broadcast and assume a higher level (such as, RAC) will retry.
			if (ret == -1) {
				std::stringstream s;
				s << "Error broadcasting message: " << pcap_geterr(pcapfd);
				std::cerr << s.str() << std::endl;
				throw std::runtime_error(s.str());
			}
		}
	} while (! stopSendingThread);
}

static void lleac_pcap_handler(u_char *user, const struct pcap_pkthdr *header, const u_char *packet)
{
	// Sometimes, PCAP returns incomplete packets. Ignore them, because they will not be able to be unpacked.
	if (header->caplen != header->len) {
		std::cerr << "WARNING: pcap received incomplete packet (LEN=" << header->len << ",CAPLEN=" << header->caplen << ")." << std::endl;
		return;
	}
	PCAPEthernetAnonymousCommunicator* lleac = (PCAPEthernetAnonymousCommunicator*) user;
	lleac->distributePacket(packet, header->caplen);
}

void PCAPEthernetAnonymousCommunicator::packetReceiver()
{
	struct bpf_program bpfProgram;
#ifdef DEBUG
	try {
#endif
		if (pcap_compile(pcapfd, &bpfProgram, "ether broadcast", 0, PCAP_NETMASK_UNKNOWN) == -1) {
			std::stringstream s;
			s << "[EAC] pcap_compile(): " << pcap_geterr(pcapfd);
			throw std::runtime_error(s.str());
		}
		if (pcap_setfilter(pcapfd, &bpfProgram) == -1) {
			std::stringstream s;
			s << "[EAC] pcap_setfilter(): " << pcap_geterr(pcapfd);
			throw std::runtime_error(s.str());
		}
		if (pcap_loop(pcapfd, -1, lleac_pcap_handler, (u_char*) this) == -1) {
			std::stringstream s;
			s << "[EAC] pcap_loop(): " << pcap_geterr(pcapfd);
			throw std::runtime_error(s.str());
		}
#ifdef DEBUG
	}
	catch (std::exception& e) {
		if (globalParameters.debugEAC > 0)
			std::cerr << "[EAC] EXCEPTION IN PCAPEthernetAnonymousCommunicator.packetReceiver(): " << e.what() << std::endl;
	}
#endif
}

#if defined(__FreeBSD__)
BPFEthernetAnonymousCommunicator::BPFEthernetAnonymousCommunicator(const std::string &networkInterface) :
		stopSendingThread(false),
		stopReceivingThread(false)
{
	struct ifreq iface;
	int option;

	// XXX Open the BPF. Needs to be root, unless a certain sysctl is set.
	if ((bpffd = open("/dev/bpf0", O_RDWR)) == -1) { // TODO: find an unused BPF
		std::stringstream s;
		s << "[EAC] Error opening BPF: " << strerror(errno);
		throw std::runtime_error(s.str());
	}
	// TODO: use more than one interface if we are multihomed
	// TODO: use a kernel filter to discard unwanted messages
	strcpy(iface.ifr_name, networkInterface.c_str());
	if (ioctl(bpffd, BIOCSETIF, &iface) == -1) {
		std::stringstream s;
		s << "[EAC] Error setting " << networkInterface << " as BPF network interface: " << strerror(errno);
		throw std::runtime_error(s.str());
	}
	// Get/set various BPF options:
	//   Set immediate mode (reads return immediately upon packet reception)
	//   Set header complete (link level source address will be written, as provided, to the wire.
	//   Get required buffer length for reads.
	option = 1;
	if (ioctl(bpffd, BIOCIMMEDIATE, &option) == -1 ||
			ioctl(bpffd, BIOCSHDRCMPLT, &option) == -1 ||
			ioctl(bpffd, BIOCGBLEN, &bpfBufferLength) == -1) {
		std::stringstream s;
		s << "[EAC] Error getting/setting BPF options: " << strerror(errno);
		throw std::runtime_error(s.str());
	}
	receivingBpfBuffer = new u_int8_t[bpfBufferLength];
	receivingPos = 0;
#ifdef SIMULATE_COMM_ERRORS
	if (globalParameters.EACFailureRate > 0)
		srand(time(NULL));
#endif
	sendingThread = boost::thread(&BPFEthernetAnonymousCommunicator::packetSender, this);
	receivingThread = boost::thread(&BPFEthernetAnonymousCommunicator::packetReceiver, this);
}

BPFEthernetAnonymousCommunicator::~BPFEthernetAnonymousCommunicator()
{
	stopReceivingThread = true;
	receivingThread.join();
	stopSendingThread = true;
	sendingQueue.stopAll();
	sendingThread.join();
	close(bpffd);
	delete receivingBpfBuffer;
}

void BPFEthernetAnonymousCommunicator::packetSender()
{
	MessageBuffer* mb;
	int ret, ret_errno;

	do {
		if (sendingQueue.pop(mb)) {
#ifdef DEBUG_EAC
			if (globalParameters.debugEAC > 0) {
				std::cerr << "[EAC] Broadcast dump (" << mb->length << "B): ";
				std::cerr << std::hex;
				u_int8_t* aux = (u_int8_t*) mb->data;
				for (unsigned int p = 0; p < mb->length; p++, aux++)
					std::cerr << (int) *aux << " ";
				std::cerr << std::dec;
				std::cerr << std::endl;
			}
#endif
			// Broadcast the message
			ret = write(bpffd, mb->data, mb->length);
			ret_errno = errno;
			delete (u_int8_t*) mb->data;
			delete mb;
			if (ret != (int) mb->length) {
				std::stringstream s;
				s << "[EAC] Error broadcasting message: " << strerror(ret_errno);
				std::cerr << s.str() << std::endl;
				throw std::runtime_error(s.str());
			}
			// TODO: in case of error, maybe we can try to restart BPF, or maybe we can discard the broadcast and assume a higher level (such as, RAC) will retry.
		}
	} while (! stopSendingThread);
}

void BPFEthernetAnonymousCommunicator::packetReceiver()
{
	struct bpf_hdr *bpfPacket;

#ifdef DEBUG
	try {
#endif
		// TODO: set a BPF filter
		do {
			// If there are no pending data to process, read more data.
			if (receivingPos == 0) {
				// TODO: use select to implement a read timeout
				if ((bytesReceived = read(bpffd, receivingBpfBuffer, bpfBufferLength)) == -1) {
					std::stringstream s;
					s << "[EAC] Error receiving data: " << strerror(errno);
					std::cerr << s.str() << std::endl;
					throw std::runtime_error(s.str());
				}
				if (! stopReceivingThread)
					receivingPos = receivingBpfBuffer;
			}
			if (! stopReceivingThread) {
				if (bytesReceived > 0) {
					// Process one BPF packet
					bpfPacket = (struct bpf_hdr*) receivingPos;
					distributePacket((u_int8_t*) bpfPacket + bpfPacket->bh_hdrlen, bpfPacket->bh_caplen);
					// Advance to the next BPF packet. If end of data is reached, set receivingPos to zero,
					// so that more data will be read next time.
					receivingPos += BPF_WORDALIGN(bpfPacket->bh_hdrlen + bpfPacket->bh_caplen);
					if (receivingPos >= (u_int8_t*) receivingBpfBuffer + bytesReceived)
						receivingPos = 0;
				}
				else
					throw std::runtime_error("[EAC] BPF read returned zero bytes (EOF)");
			}
		} while (! stopReceivingThread);
#ifdef DEBUG
	}
	catch (std::exception& e) {
		if (globalParameters.debugEAC > 0)
			std::cerr << "[EAC] EXCEPTION IN BPFEthernetAnonymousCommunicator.packetReceiver(): " << e.what() << std::endl;
	}
#endif
}
#endif

EthernetAnonymousCommunicator::EthernetAnonymousCommunicator(int serviceTag, MessageFactory& messageFactory, const std::string& networkInterface, MessageQueue& messageReceivingQueue) :
	serviceTag(serviceTag),
	stopReceivingThread(false),
	messageFactory(messageFactory),
	messageReceivingQueue(messageReceivingQueue)
{
	if (networkInterface.size() == 0)
		networkInterfaceName = globalParameters.anonymousInterface;
	else
		networkInterfaceName = networkInterface;
	boost::lock_guard<boost::mutex> m(llEACMapMutex);
	if (llEACMap.count(networkInterfaceName) == 1) {
		LowLevelEthernetAnonymousCommunicatorUsers& llEACUsers = llEACMap[networkInterfaceName];
		llEACUsers.numUsers++;
		llEAC = llEACUsers.llEAC;
	}
	else {
		LowLevelEthernetAnonymousCommunicatorUsers llEACUsers;
		llEACUsers.numUsers = 1;
#if ! defined(__FreeBSD__) || defined(USE_LIBPCAP)
		llEACUsers.llEAC = new PCAPEthernetAnonymousCommunicator(networkInterfaceName);
#else
		llEACUsers.llEAC = new BPFEthernetAnonymousCommunicator(networkInterfaceName);
#endif
		llEAC = llEACUsers.llEAC;
	}
	llEAC->addReceivingQueue(this->serviceTag, &messageBufferReceivingQueue);
	receivingThread = boost::thread(&EthernetAnonymousCommunicator::messageReceiver, this);
}

EthernetAnonymousCommunicator::~EthernetAnonymousCommunicator()
{
	stopReceivingThread = true;
	messageBufferReceivingQueue.stopAll();
	receivingThread.join();
	llEAC->dropReceivingQueue(&messageBufferReceivingQueue);
	boost::lock_guard<boost::mutex> m(llEACMapMutex);
	LowLevelEthernetAnonymousCommunicatorUsers& llEACUsers = llEACMap[networkInterfaceName];
	llEACUsers.numUsers--;
	if (llEACUsers.numUsers == 0) {
		delete llEAC;
		llEACMap.erase(networkInterfaceName);
	}
}

void EthernetAnonymousCommunicator::broadcast(const Message* const message)
{
#ifdef SIMULATE_COMM_ERRORS
	if (globalParameters.EACFailureRate > 0)
		if ((rand() % 100) < globalParameters.EACFailureRate) {
			if (globalParameters.debugEAC > 0)
				std::cerr << "[EAC] *** MESSAGE DISCARDED DUE TO SIMULATED COMMS ERROR ***" << std::endl;
			return;
		}
#endif
	const MessageBuffer* mb = message->pack();
	// Assemble the ethernet header
	struct ether_header etherHeader;
	memset(&etherHeader.ether_dhost, 0xff, ETHER_ADDR_LEN); // Destination: broadcast address.
	memset(&etherHeader.ether_shost, 0x01, ETHER_ADDR_LEN); // Source: a fictional address.
	etherHeader.ether_type = htons(AC_ETHERTYPE);
	// Assemble the AC protocol header
	EACHeader eacHeader;
	eacHeader.serviceTag = serviceTag;
	eacHeader.payloadDataLength = 0;
	const MessageBuffer* mb1 = mb;
	while (mb1 != 0) {
		eacHeader.payloadDataLength += mb1->length;
		mb1 = mb1->prev;
	}

	#if DEBUG_SMSG > 1
	std::cout << std::endl;
    std::cout << " ------------------------------" << std::endl;
//    std::cout << " -- Message Broadcast: " << " Length: " << mb->length <<  " Data: " << mb-> data <<std::endl;
    std::cout << "[SMSG] <<-- Message Broadcasting -->>" << std::endl;
    std::cout << " ------------------------------" << std::endl;
    #endif

	eacHeader.payloadDataLength = htons(eacHeader.payloadDataLength);
	// Broadcast the message
	llEAC->broadcast(etherHeader, eacHeader, mb);
	delete (u_int8_t*) mb->data;
	delete mb;
}

void EthernetAnonymousCommunicator::messageReceiver()
{
#ifdef DEBUG
	try {
#endif
		do {
			MessageBuffer* mb;
			if (messageBufferReceivingQueue.pop(mb)) {

				Message* m = messageFactory.produce(mb);
				if (m != 0)
                {
                 messageReceivingQueue.push(m);
                }
				delete (u_int8_t*) mb->data;
				delete mb;
			}
		} while (! stopReceivingThread);
#ifdef DEBUG
	}
	catch (std::exception& e) {
		if (globalParameters.debugEAC > 0)
			std::cerr << "[EAC] EXCEPTION IN EthernetAnonymousCommunicator.messageReceiver(): " << e.what() << std::endl;
	}
#endif
}


