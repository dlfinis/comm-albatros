#####################################################################
### Declaración de macros de los ficheros de codigo fuente,
### objeto, cabecera y ejecutable.
###
MODULOS = AnonymousCommunicator.cpp EthernetAnonymousCommunicator.cpp Message.cpp GlobalParameters.cpp Util.cpp TestType.cpp SecurityMessage.h
PROGRAMAS = CommsTest.cpp EjemploCharla.cpp
EJECUTABLES = CommsTest albatros

#####################################################################

#####################################################################
### Opciones para el compilador y enlazador.
###

INCLUDES = -I/usr/local/include
BIBLIOTECAS = -L/usr/local/lib -lboost_system -lboost_thread -lpcap -lcrypto++
OPCIONES_CODIGO = -DSIMULATE_COMM_ERRORS -DUSE_LIBPCAP
OPCIONES_DEPURACION = -g -DDEBUG -DDEBUG_RAC -DDEBUG_EAC -DDEBUG_SMSG
CFLAGS = $(OPCIONES_DEPURACION)
CXX = c++
CXXFLAGS =  -std=c++11 $(INCLUDES) -Wall $(OPCIONES_CODIGO) $(OPCIONES_DEPURACION)
LDFLAGS = $(BIBLIOTECAS)

#####################################################################


#####################################################################
### Declaración de reglas y dependencias

all: $(EJECUTABLES)

todo: all

limpia: clean

doc: Doxyfile $(MODULOS) $(MODULOS:.cpp=.h)
	doxygen

-include ".depend"

CommsTest: CommsTest.o AnonymousCommunicator.o EthernetAnonymousCommunicator.o Message.o GlobalParameters.o Util.o TestType.o
	c++ $(CFLAGS) -o $@ $^ $(LDFLAGS)

albatros: EjemploCharla.o AnonymousCommunicator.o EthernetAnonymousCommunicator.o Message.o GlobalParameters.o Util.o TestType.o SecurityMessage.h
	c++ $(CFLAGS) -o $@ $^ $(LDFLAGS)

clean:;
#	rm -f $(MODULOS:.cpp=.o) $(PROGRAMAS:.cpp=.o) *core *~ *.bak $(EJECUTABLES)
	rm -f *.o $(EJECUTABLES)

depend:;
	@gcc -E -MM $(MODULOS) > .depend
	@echo "Lista de dependencias creada"

#####################################################################
## Uso:
# make	-> Compila y genera el ejecutable si no hay error
# make clean o make limpia -> Borra los ficheros objeto, ejecutable y cores
# make depend -> Genera la lista de dependencias fuente->objeto
#####################################################################
