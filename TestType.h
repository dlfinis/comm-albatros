/**
 * Test data type.
 *
 */

#ifndef _TESTTYPE_H_
#define _TESTTYPE_H_

#include <string>
#include "Message.h"

class TestMessage: public Message {
public:
	TestMessage();
	TestMessage(int number, const std::string& text);
	~TestMessage();
	MessageBuffer* pack() const;
	Message* unpack(const MessageBuffer* buf);
	int getNumber() const;
	const std::string& getText() const;
	std::string toString() const;
	const static MessageType type_id = 1;
private:
	int number;
	std::string text;
};
#endif
