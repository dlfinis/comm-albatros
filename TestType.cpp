#include "TestType.h"
#include "SecurityMessage.h"
#include <iostream>
#include <iomanip>
#include <sstream>


SecurityMessage sec;

using namespace std;

TestMessage::TestMessage()
	: Message(TestMessage::type_id)
{
}

TestMessage::TestMessage(int number, const std::string& text)
	: Message(TestMessage::type_id)
{
	this->number = number;
    this->text = text;
}


TestMessage::~TestMessage()
{
}

int TestMessage::getNumber() const {
	return number;
}

const std::string& TestMessage::getText() const
{
	return text;
}

std::string TestMessage::toString() const
{
	std::stringstream s;
	s << "[" << number << ",[" << text << "]";
	return s.str();
}


MessageBuffer* TestMessage::pack() const {
	MessageBuffer* mb0;
	mb0 = Message::pack();
	MessageBuffer* mb = new MessageBuffer;

    string ctext = sec.pack(text);
	mb->length = sizeof(int) + ctext.length() + 1;
	mb->data = new u_int8_t[mb->length];
	memcpy(mb->data, &number, sizeof(int));

	strncpy((char*) ((u_int8_t*) mb->data + sizeof(int)), ctext.c_str(), mb->length - sizeof(int));
	mb->prev = mb0;

    #if DEBUG_SMSG > 2
    std::cout << endl;
//    std::cout << " ------------------------------" << endl;
        std::cout << "[SMSG] --> Message " << endl;
//        std::cout << " --> " << text << endl;
        sec.printHex(text);

        std::cout << "[SMSG] --> Encrypted Message " << endl;
//        std::cout << " --> " << ctext << endl;
        sec.printHex(ctext);
    std::cout << " ------------------------------" << endl;
	#endif
	return mb;
}

Message* TestMessage::unpack(const MessageBuffer* buf) {
	int* number;
	char* text;
    std::string mn;
	// XXX Assumes the message comes in a single chunk.
	if (*((MessageType *)buf->data) != type_id)
		throw std::logic_error("TestMessage: attempt to unpack a different message type");
	// if (buf->length != sizeof(MessageType) + sizeof(int))
	//	throw std::logic_error("TestMessage: attempt to unpack an invalid message");
	number = (int *)((u_int8_t*) buf->data + sizeof(MessageType));

	text = (char *)((u_int8_t*) buf->data + sizeof(MessageType) + sizeof(int));
    mn = sec.unpack(std::string(text));


	#if DEBUG_SMSG > 2
        std::cout << endl;
//        std::cout << " ------------------------------" << endl;
        std::cout << "[SMSG] --> Encrypted Message " << endl;
//        std::cout << " --> " << std::string(text) << endl;
        sec.printHex(text);

        std::cout << "[SMSG] --> Decrypted Message" << endl;
//        std::cout << " --> " << mn << endl;
        sec.printHex(mn);
        std::cout << " ------------------------------" << endl;
	#endif

	TestMessage* m = new TestMessage(*number, mn);
	return m;
}

