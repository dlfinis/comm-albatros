/**
 * Anonymous communication test program.
 *
 */

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <sys/types.h>
#include <net/ethernet.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "EthernetAnonymousCommunicator.h"
#include "TestType.h"

using namespace std;


#define SERVICE_TAG 92
#define AC_ETHERTYPE 0x88b5

MessageFactory messageFactory;
MessageQueue mq;
boost::thread receivingThread;

void eacMessageReceiver(MessageQueue* const mq)
{
	bool finish = false;

	do {
		Message* maux;
		if (mq->pop(maux)) {
			TestMessage* m = (TestMessage*) maux;
			if (m->getNumber() != getpid())
				std::cout << ">> " << m->getText() << " " << std::endl << std::flush;
		}
		else
			finish = true;
	} while (! finish);
}

AnonymousCommunicator* startCommunications(int argc, char** argv)
{
	AnonymousCommunicator* communicator;

	if (! globalParameters.parse(argc, argv) || globalParameters.anonymousInterface.length() == 0) {
		globalParameters.usage();
		std::cerr << "REQUIREMENTS TO RUN THIS PROGRAM:\n\n";
		std::cerr << " * You must define the network interface for anonymous communications (e.g., \"eth0\", \"age0\"...)\n";
		std::cerr << "\nIn order to be able to use anonymous communications, you'll probably have to run this as root, as the program needs to be able to generate raw ethernet frames." << std::endl;
		exit(1);
	}
	messageFactory.registerType(TestMessage::type_id, (UnpackerFunction) new TestMessage());
	communicator = new EthernetAnonymousCommunicator(SERVICE_TAG, messageFactory, globalParameters.anonymousInterface, mq);
	if (globalParameters.initialSleep > 0) {
		std::cerr << "Sleeping for " << globalParameters.initialSleep << " seconds..." << std::endl;
		sleep(globalParameters.initialSleep);
	}
	receivingThread = boost::thread(eacMessageReceiver, &mq);
	return communicator;
}


void chat(AnonymousCommunicator* communicator)
{
	bool quit_chat;
	std::string comment;

	quit_chat = false;
	do {
		std::cout << " > " << std::flush;
		if (! getline(cin, comment) || comment == "quit")
			quit_chat = true;
		else {
			TestMessage msg(getpid(), comment);
			communicator->broadcast(&msg);
		}
	} while (! quit_chat);
}

void terminateCommunications(AnonymousCommunicator* communicator)
{
	mq.stopAll();
	receivingThread.join();
	delete communicator;
}

int main(int argc, char** argv)
{
	AnonymousCommunicator* communicator;
	try {
		communicator = startCommunications(argc, argv);
		chat(communicator);
		terminateCommunications(communicator);
		return 0;
	}
	catch (exception& e) {
		std::cerr << "EXCEPTION: " << e.what() << std::endl;
	}
}
