/**
 * Anonymous communication test program.
 *
 */

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <sys/types.h>
#include <net/ethernet.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "EthernetAnonymousCommunicator.h"
#include "TestType.h"

using namespace std;


#define SERVICE_TAG 92
#define AC_ETHERTYPE 0x88b5


void eacMessageReceiver(MessageQueue* const mq)
{
	bool finish = false;

	do {
		Message* maux;
		if (mq->pop(maux)) {
			TestMessage* m = (TestMessage*) maux;
			std::cout << "PID=" << getpid() << " received message:" << std::endl;
			std::cout << "  number=" << m->getNumber() << ", text=" << m->getText() << std::endl << std::flush;
		}
		else
			finish = true;
	} while (! finish);
}

int testEAC(int argc, char** argv)
{
	if (! globalParameters.parse(argc, argv) || globalParameters.anonymousInterface.length() == 0) {
		globalParameters.usage();
		std::cerr << "REQUIREMENTS TO RUN THIS PROGRAM:\n\n";
		std::cerr << " * You must define the network interface for anonymous communications (e.g., \"eth0\", \"age0\"...)\n";
		std::cerr << "\nIn order to be able to use anonymous communications, you'll probably have to run this as root, as the program needs to be able to generate raw ethernet frames." << std::endl;
		exit(1);
	}
	MessageFactory messageFactory;
	messageFactory.registerType(TestMessage::type_id, (UnpackerFunction) new TestMessage());
	EthernetAnonymousCommunicator* a;
	MessageQueue mq;
	a = new EthernetAnonymousCommunicator(SERVICE_TAG, messageFactory, globalParameters.anonymousInterface, mq);
	if (globalParameters.initialSleep > 0) {
		std::cerr << "Sleeping for " << globalParameters.initialSleep << " seconds..." << std::endl;
		sleep(globalParameters.initialSleep);
	}
	boost::thread receivingThread = boost::thread(eacMessageReceiver, &mq);
	for (int i = 1; i <= 1000; i++) {
		stringstream s;
		s << getpid() << "-TEST-" << i;
		TestMessage msg(getpid(), s.str());
		a->broadcast(&msg);
		usleep(1000);
	}
	std::cout << "PID=" << getpid() << " sent his messages. Sleeping for a few additional seconds..." << std::endl;
	sleep(10);
	std::cout << "CommsTest ended" << std::endl;
	mq.stopAll();
	receivingThread.join();
	delete a;
	return 0;
}

int main(int argc, char** argv)
{
	try {
		return testEAC(argc, argv);
	}
	catch (exception& e) {
		std::cerr << "EXCEPTION: " << e.what() << std::endl;
	}
}
