/**
 * Messaging classes.
 *
 * @author Javier Martín Rueda (jmrueda@diatel.upm.es)
 * @version 1.0 (18-jun-2014)
 */

#ifndef _MESSAGES_H_
#define _MESSAGES_H_

#include <sys/types.h>
#include <map>
#include <queue>
#include <boost/thread/thread.hpp>
#include <boost/thread/locks.hpp>
#include "Util.h"

typedef u_int8_t MessageType;

/**
 * Message buffer.
 */
typedef struct _MessageBuffer {
	unsigned int length; /**< Buffer length in bytes. */
	void* data; /**< Buffer data. */
	struct _MessageBuffer* prev; /**< Pointer to previous message buffer. */
	// TODO: implement delete, which has to free both mb->data and mb, recursively.
} MessageBuffer;

/**
 * Base message class.
 */
class Message {
public:
	/**
	 * Creates a message.
	 */
	Message(MessageType type);

	/**
	 * Destroys a message.
	 */
	virtual ~Message();

	/**
	 * Serializes a message so that it can be transmitted over the wire.
	 * @return Array of message buffers with the serialized message.
	 */
	virtual MessageBuffer* pack() const;

	/**
	 * Creates a new message of this type from the serialized data which has been received over the wire.
	 * @return A new message of this type.
	 * @throws logic_error Attempt to unpack an invalid message.
	 */
	virtual Message* unpack(const MessageBuffer* buf);

	/**
	 * Returns the message type.
	 * @return Message type.
	 */
	MessageType getType() const;

private:
	MessageType type; // Message type
};

// typedef Message* (Message::*UnpackerFunction)(const MessageBuffer*);
typedef Message* UnpackerFunction;
typedef std::map<MessageType,UnpackerFunction> UnpackersMap;

/**
 * Objects of this class are used to decode messages received over the wire.
 * In order to use it, you have to register possible message types and corresponding unpacker objects.
 */
class MessageFactory {
public:
	/**
	 * Register a message type.
	 * @param type Identifier of this message type.
	 * @param unpacker A subclass of Message object that can unpack messages of this type.
	 */
	void registerType(MessageType type, UnpackerFunction unpacker);
	/**
	 * Produce a new message.
	 * @param messageBuffer Message buffer with the unpacked message.
	 */
	Message* produce(const MessageBuffer* messageBuffer);
private:
	UnpackersMap unpackers;
};
// TODO: for the moment, MessageFactory needs an instanced object of each message class to unpack. Should be changed to function pointers or similar.

typedef BlockingQueue<MessageBuffer*> MessageBufferQueue;
typedef std::list<MessageBufferQueue*> MessageBufferQueueList;
typedef std::map<int,MessageBufferQueueList*> MessageBufferQueuesMap;

typedef BlockingQueue<Message*> MessageQueue;

#endif

