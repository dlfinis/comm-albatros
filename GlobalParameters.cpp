#include <iostream>
#include <getopt.h>
#include <stdlib.h>
#include "GlobalParameters.h"

GlobalParameters globalParameters;

GlobalParameters::GlobalParameters() :
		numProcesses(0),
		anonymousInterface(""),
		initialSleep(0),
		debugRAC(0),
		debugEAC(0),
		debugConsensus(0),
		debugFD(0),
		debugSMSG(0),
		EACFailureRate(0),
		racRebroadcastTimeoutMs(2000)
{
	// This sleep timer is to allow all participating processes to wait until ready to receive messages.
	// Otherwise, some messages could be lost and the protocol may not work because it assumes communications
	// are reliable.
	if (getenv("INITIAL_SLEEP") != NULL)
		initialSleep = atoi(getenv("INITIAL_SLEEP"));
	if (getenv("ANONYMOUS_INTERFACE") != NULL)
		anonymousInterface.assign(getenv("ANONYMOUS_INTERFACE"));
	if (getenv("NUM_PROCESSES") != NULL)
		numProcesses = atoi(getenv("NUM_PROCESSES"));
	if (getenv("DEBUG_RAC") != NULL)
		debugRAC = atoi(getenv("DEBUG_RAC"));
	if (getenv("DEBUG_EAC") != NULL)
		debugEAC = atoi(getenv("DEBUG_EAC"));
	if (getenv("DEBUG_CONSENSUS") != NULL)
		debugConsensus = atoi(getenv("DEBUG_CONSENSUS"));
	if (getenv("DEBUG_FD") != NULL)
		debugFD = atoi(getenv("DEBUG_FD"));
	if (getenv("DEBUG_SMSG") != NULL)
		debugSMSG = atoi(getenv("DEBUG_SMSG"));
	if (getenv("DEBUG_TWITTER") != NULL)
		debugTwitter = atoi(getenv("DEBUG_TWITTER"));
	if (getenv("EAC_FAILURE_RATE") != NULL)
		EACFailureRate = atoi(getenv("EAC_FAILURE_RATE"));
	if (getenv("RAC_REBROADCAST_TIMEOUT") != NULL)
		racRebroadcastTimeoutMs = atoi(getenv("RAC_REBROADCAST_TIMEOUT"));
}

GlobalParameters::~GlobalParameters() {
}

bool GlobalParameters::parse(int argc, char** argv) {
	while (1) {
		int option_index = 0;
		static struct option long_options[] = {
				{"numproc", required_argument, 0, 'n' },
				{"interface", required_argument, 0, 'i' },
				{"sleep", required_argument, 0, 's' },
				{"debugRAC", optional_argument, 0, '0' },
				{"debugEAC", optional_argument, 0, '1' },
				{"debugConsensus", optional_argument, 0, '2' },
				{"debugFD", optional_argument, 0, '3' },
				{"debugSMSG", optional_argument, 0, '4' },
				{"debugTwitter", optional_argument, 0, '5' },
				{"EACfail", required_argument, 0, 'f' },
				{"RACtimeout", required_argument, 0, 'r' }
		};
        int c = getopt_long(argc, argv, "", long_options, &option_index);
        if (c == -1)
            break;
        switch (c) {
        case '0': globalParameters.debugRAC = atoi(optarg); break;
        case '1': globalParameters.debugEAC = atoi(optarg); break;
        case '2': globalParameters.debugConsensus = atoi(optarg); break;
        case '3': globalParameters.debugFD = atoi(optarg); break;
        case '4': globalParameters.debugSMSG = atoi(optarg); break;
        case '5': globalParameters.debugTwitter = atoi(optarg); break;
        case 'f': globalParameters.EACFailureRate = atoi(optarg); break;
        case 'i': globalParameters.anonymousInterface.assign(optarg); break;
        case 'n': globalParameters.numProcesses = atoi(optarg); break;
        case 'r': globalParameters.racRebroadcastTimeoutMs = atoi(optarg); break;
        case 's': globalParameters.initialSleep = atoi(optarg); break;
        default: return false;
        }
	}
	return true;
}

void GlobalParameters::usage() const {
	std::cerr << "Available general options: \n\n";
	std::cerr << "  --numproc=NUM  (total number of processes in the system)\n";
	std::cerr << "  --interface=NETWORK_INTERFACE  (network interface for anonymous communications)\n";
	std::cerr << "  --sleep=SECONDS  (if set, the program will wait this many seconds before starting to do things)\n";
	std::cerr << "  --debugRAC=LEVEL  (debugging level for ReliableAnonymousCommunicator)\n";
	std::cerr << "  --debugEAC=LEVEL  (debugging level for EthernetAnonymousCommunicator)\n";
	std::cerr << "  --debugConsensus=LEVEL  (debugging level for Consensus)\n";
	std::cerr << "  --debugSMSG=LEVEL  (debugging level for EthernetAnonymousCommunicator)\n";
	std::cerr << "  --debugFD=LEVEL  (debugging level for FailureDetector)\n";
	std::cerr << "  --debugTwitter=LEVEL  (debugging level for AnonTwitter)\n";
	std::cerr << "  --EACfail=PERCENTAGE  (simulated failure rate for EAC broadcasts)\n";
	std::cerr << "  --RACtimeout=MILLISECONDS  (timeout for rebroadcasting)\n";
	std::cerr << "\n";
	std::cerr << "The following environment variables can also be set. They are overriden by the options above.\n\n";
	std::cerr << "  ANONYMOUS_INTERFACE : network interface for anonymous communications\n";
	std::cerr << "  INITIAL_SLEEP : if set, the program will wait this many seconds before starting to do things\n";
	std::cerr << "  NUM_PROCESSES : total number of processes in the system\n";
	std::cerr << "  DEBUG_RAC : debugging level for ReliableAnonymousCommunicator\n";
	std::cerr << "  DEBUG_EAC : debugging level for EthernetAnonymousCommunicator\n";
	std::cerr << "  DEBUG_CONSENSUS : debugging level for Consensus\n";
	std::cerr << "  DEBUG_FD : debugging level for FailureDetector\n";
	std::cerr << "  DEBUG_SMSG : debugging level for SecurityMessage\n";
	std::cerr << "  DEBUG_TWITTER : debugging level for AnonTwitter\n";
	std::cerr << "  EAC_FAILURE_RATE : simulated failure rate for EAC broadcasts\n";
	std::cerr << "  RAC_REBROADCAST_TIMEOUT : timeout for rebroadcasting\n";
	std::cerr << "\n";
	std::cerr << std::endl;
}

