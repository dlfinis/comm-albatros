/**
 * Anonymous communication through ethernet frames.
 *
 * @author Javier Martín Rueda (jmrueda@diatel.upm.es)
 * @version 1.0 (18-jun-2014)
 */

#ifndef _ETHERNET_ANONYMOUS_COMMUNICATOR_H_
#define _ETHERNET_ANONYMOUS_COMMUNICATOR_H_

#include <pcap.h>
#include <list>
#include <boost/thread/thread.hpp>
#include <boost/thread/locks.hpp>
#include "GlobalParameters.h"
#include "AnonymousCommunicator.h"
#include "Message.h"
#include "Util.h"

/**
 * This class is used to perform anonymous communication (both sending and receiving messages).
 * Currently, it works by encapsulating the messages in ethernet frames and broadcasting them in
 * the LAN segment with a ficticious ethernet source address, so that the sender cannot be easily
 * identified. Note that an observer that can monitor the ethernet switch may be able to identify
 * parties in an anonymous communication by checking the source switch port for anonymous frames.
 *
 * In order not to disturb other ethernet communications, a specific ethernet frame type is used.
 * Frames are also tagged with a service identifier to be able to distinguish frames for different
 * protocols.
 *
 * The generic format of an anonymous ethernet frame is:
 *
 * | MAC destination (FF:FF:FF:FF:FF:FF) | MAC source (11:11:11:11:11:11) | Ethertype (2 bytes) | Service tag (1 byte) | Payload |
 */

/**
 * Header for ethernet anonymous communicator frames.
 */
// TODO: think about the possibility of adding a checksum to detect bogus frames.
typedef struct {
	ServiceType serviceTag; /**< Service tag (protocol identifier) */
	u_int16_t payloadDataLength; /**< Payload data length (packet may be larger because of padding) */
} EACHeader;

/**
 * Interface for a low-level Ethernet anonymous communicator. The idea is to have a single low-level EAC
 * for each network interface. This low-level EAC would be shared among all EAC instances.
 * An EAC instance broadcasts messages by invoking the broadcast method. On return, the message is not
 * necessarily broadcasted, but rather a copy of it has been pushed into an outgoing queue.
 * The original message buffer can be freed or modified by the sender at will.
 * The low-level EAC has a thread which takes messages from the outgoing queue and broadcasts them on the wire.
 * An EAC instance receives messages through an incoming queue. The incoming queue is created by the EAC instance
 * and supplied to the low-level EAC, together with a service type identifier to filter messages, by invoking
 * the addReceivingQueue method. Messages in the queue must be freed by the receiver when not needed any longer.
 */
class LowLevelEthernetAnonymousCommunicator {
public:
	/**
	 * Destroy a low-level Ethernet anonymous communicator.
	 */
	virtual ~LowLevelEthernetAnonymousCommunicator();
	/**
	 * Anonymously broadcast a message buffer. On return, the message is not
	 * necessarily broadcasted, but rather a copy of it has been pushed into an outgoing queue.
	 * The original message buffer can be freed or modified by the sender at will.
	 * @param etherHeader Ethernet header.
	 * @param eacHeader EAC Header.
	 * @param message Message buffer to be broadcasted.
	 * @throws runtime_error Error in some network system call.
	 */
	void broadcast(const struct ether_header& etherHeader, const EACHeader& eacHeader, const MessageBuffer* const message);
	/**
	 * Add a message buffer receiving queue for a certain service type. This low-level Ethernet anonymous communicator
	 * will push received messages for that service type into the queue. Messages in the queue must be freed
	 * by the receiver when not needed any longer.
	 * @param serviceTag Service type.
	 * @param messageBufferQueue Message buffer receiving queue.
	 */
	void addReceivingQueue(const int serviceTag, MessageBufferQueue* const messageBufferQueue);
	/**
	 * Remove a message buffer receiving queue. The queue is not modified, but no new messages will be pushed into it.
	 * @param Message buffer queue to be removed.
	 */
	void dropReceivingQueue(MessageBufferQueue* const messageBufferQueue);
	/**
	 * Distribute an ethernet packet to all receiving queues. This method is to be used by the PCAP handler only.
	 * @param packet Pointer to the Ethernet packet (including ethernet header).
	 * @param packet_length Packet length in bytes.
	 */
	void distributePacket(const void* const packet, unsigned int packet_length);
protected:
	MessageBufferQueue sendingQueue;
private:
	MessageBufferQueuesMap receivingQueues;
	boost::mutex receivingQueuesMutex;
};

struct LowLevelEthernetAnonymousCommunicatorUsers {
	LowLevelEthernetAnonymousCommunicator* llEAC;
	int numUsers;
};
typedef std::map<std::string,LowLevelEthernetAnonymousCommunicatorUsers> LowLevelEthernetAnonymousCommunicatorMap;

class PCAPEthernetAnonymousCommunicator : public LowLevelEthernetAnonymousCommunicator {
public:
	/**
	 * Create a PCAP-based low-level Ethernet anonymous communicator operating on a certain network interface.
	 * @param networkInterface The name of the network interface.
	 */
	PCAPEthernetAnonymousCommunicator(const std::string& networkInterface);
	/**
	 * Destroy a PCAP-based Ethernet anonymous communicator, closing communication and releasing other resources.
	 */
	virtual ~PCAPEthernetAnonymousCommunicator();
private:
	/**
	 * Implements the packet sending thread.
	 */
	void packetSender();
	/**
	 * Implements the packet receiving thread.
	 */
	void packetReceiver();
	boost::thread sendingThread;
	bool stopSendingThread;
	boost::thread receivingThread;
	pcap_t* pcapfd;
};

#if defined(__FreeBSD__)
class BPFEthernetAnonymousCommunicator : public LowLevelEthernetAnonymousCommunicator {
public:
	/**
	 * Create a BPF-based low-level Ethernet anonymous communicator operating on a certain network interface.
	 * @param networkInterface The name of the network interface.
	 */
	BPFEthernetAnonymousCommunicator(const std::string& networkInterface);
	/**
	 * Destroy a BPF-based Ethernet anonymous communicator, closing communication and releasing other resources.
	 */
	virtual ~BPFEthernetAnonymousCommunicator();
	/**
	 * Implements the packet sending thread.
	 */
	void packetSender();
	/**
	 * Implements the packet receiving thread.
	 */
	void packetReceiver();
	boost::thread sendingThread;
	bool stopSendingThread;
	bool stopReceivingThread;
	boost::thread receivingThread;
	int bpffd;
	int bpfBufferLength;
	u_int8_t *receivingBpfBuffer;
	u_int8_t *receivingPos;
	int bytesReceived;
};
#endif

class EthernetAnonymousCommunicator : public AnonymousCommunicator {
public:
	/**
	 * Create an Ethernet anonymous communicator operating on a certain network interface.
	 * @param serviceTag Service identifier for the messages.
	 * @param messageFactory Message factory that is able to unpack messages for this protocol.
	 * @param networkInterface The name of the network interface.
	 * @param messageReceivingQueue Queue where received messages will be put.
	 */
	EthernetAnonymousCommunicator(int serviceTag, MessageFactory& messageFactory, const std::string& networkInterface, MessageQueue& messageReceivingQueue);
	/**
	 * Destroy an ethernet anonymous communicator, closing communication and releasing other resources.
	 */
	virtual ~EthernetAnonymousCommunicator();
	/**
	 * Broadcast an anonymous message.
	 * @param message Message to be broadcasted.
	 * @throws runtime_error Error in some network system call.
	 */
	void broadcast(const Message* const message);
private:
	/**
	 * Implements the message receiving thread.
	 */
	void messageReceiver();
	ServiceType serviceTag;
	volatile bool stopReceivingThread; // Used to signal the receiving thread to stop.
	boost::thread receivingThread;
	MessageFactory& messageFactory;
	static LowLevelEthernetAnonymousCommunicatorMap llEACMap;
	static boost::mutex llEACMapMutex;
	LowLevelEthernetAnonymousCommunicator* llEAC;
	MessageBufferQueue messageBufferReceivingQueue;
	MessageQueue& messageReceivingQueue;
	std::string networkInterfaceName;
};

#endif

