/**
 * Anonymous communication.
 *
 * @author Javier Martín Rueda (jmrueda@diatel.upm.es)
 * @version 1.0 (18-jun-2014)
 */

#include "AnonymousCommunicator.h"

AnonymousCommunicator::~AnonymousCommunicator()
{
}

