/**
 * Messaging classes.
 *
 * @author Javier Martín Rueda (jmrueda@diatel.upm.es)
 * @version 1.0 (18-jun-2014)
 */

#include "Message.h"
#include <stdexcept>
#include <iostream>
#include <string.h>

MessageType Message::getType() const
{
	return type;
}

Message::Message(MessageType type)
{
	this->type = type;
}

Message::~Message()
{
}

MessageBuffer* Message::pack() const
{
	MessageBuffer* mb = new MessageBuffer;
	mb->data = new MessageType;
	memcpy(mb->data, &type, sizeof(MessageType));
	mb->length = sizeof(MessageType);
	mb->prev = 0;
	return mb;
}

Message* Message::unpack(const MessageBuffer* buf)
{
	throw std::logic_error("Attempt to unpack a generic message.");
}

void MessageFactory::registerType(MessageType type, UnpackerFunction unpacker)
{
	unpackers[type] = unpacker;
}

Message* MessageFactory::produce(const MessageBuffer* messageBuffer)
{
	// XXX Assumes the message buffer consists of a single chunk.
#ifdef NODEF
	UnpackerFunction f = unpackers[*((MessageType *)messageBuffer->data)];
	Message* m;
	return (m->*f)(messageBuffer);
#endif
	Message* m = unpackers[*((MessageType *)messageBuffer->data)];
	if (m != 0)
		return (m->unpack(messageBuffer));
	else {
		std::cerr << "WARNING: ignoring unknown message type." << std::endl;
//		abort();
		return 0;
	}
}

