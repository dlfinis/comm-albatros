/**
 * Global parameters for anonymous communications.
 *
 * @author Javier Martín Rueda (jmrueda@diatel.upm.es)
 * @version 1.0 (20-nov-2014)
 */

#ifndef _GLOBALPARAMETERS_H_
#define _GLOBALPARAMETERS_H_

#include <string>

class GlobalParameters {
public:
	GlobalParameters();
	~GlobalParameters();
	int numProcesses;
	std::string anonymousInterface;
	int initialSleep;
	int debugRAC, debugEAC, debugConsensus, debugFD, debugSMSG,debugTwitter;
	int EACFailureRate; // 0 to 100
	int racRebroadcastTimeoutMs;
	/**
	 * Parse command-line arguments for global parameters. On return, global variable 'optind' will point to the next available argument (if any).
	 * @return True if no errors detected, false otherwise.
	 */
	bool parse(int argc, char**argv);
	/**
	 * Prints a usage message on stderr.
	 */
	void usage() const;
};

extern GlobalParameters globalParameters;

#endif
